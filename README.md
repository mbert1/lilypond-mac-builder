# lilypond-mac-builder

This repository contains build scripts and tools for 64-bit Mac .app versions of [LilyPond](http://lilypond.org).
I hope to get the 64-bit Mac .app downloads onto the main LilyPond website eventually, but for the moment, they're available at https://bintray.com/marnen/lilypond-darwin-64.

See [INSTALL.md](INSTALL.md) for build instructions.
