# Build instructions

The build requires MacPorts. Install it if it is not yet installed.

There are some assumptions made in the Makefile.
If you have installed MacPorts in /opt/local (default) and require root privileges when running `port`, you will need to override some variables when running `make` (see below).

Now install the software dependencies:

```
# with MacPorts under /usr/local
make MACPORTS_ROOT=/opt/local PORT="sudo /opt/local/bin/port" LN="sudo ln" setup-dependencies
# with MacPorts under ./macports
make setup-dependencies
```

This needs to be run only once and takes quite a while.

Now you can build:

```
# with MacPorts under /usr/local
make MACPORTS_ROOT=/opt/local PORT="sudo /opt/local/bin/port" LN="sudo ln" lilypond-all
# with MacPorts under ./macports
make lilypond-all
```

For further information, see See [prerequisites.md](prerequisites.md) for a detailed description of the build prerequisites and [notes.md](notes.md) for misc notes concerning the build.
